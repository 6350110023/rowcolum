import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Row & Column',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('My Row and Column'),
        ),
        body: Container(
          alignment: FractionalOffset.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 80,
                backgroundImage: AssetImage('assets/nat.jpg'),
              ),
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child:  Text(
                  "Hello, \nNattapat ",
                  style: TextStyle(fontSize: 25),
                   textAlign: TextAlign.start,
              ),
               ),
              // Text(
              //   "Nattapat",
              //   style: TextStyle(fontSize: 25),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
